import express from 'express';
export const router = express.Router();
import conexion from '../models/conexion.js';
import alumnosDb from "../models/alumnos.js";
import { render } from 'ejs';





router.get('/', (req, res) => {
    res.render('index', { titulo: "Mi Primer Pagina ejs", nombre: "Oscar alejandro Solis velarde" });
});

router.get('/node', (req, res) => {
    res.render('node/index', { titulo: "Mi Primer Pagina ejs", nombre: "Oscar alejandro Solis velarde" });
});
router.get('/preexamen', (req, res) => {
    res.render('preexamen/index', { titulo: "Mi Primer Pagina ejs", nombre: "Oscar alejandro Solis velarde" });
});
router.get('/node/tabla', (req, res) => {
    const params = {
        numero: req.query.numero
    }
    res.render('node/tabla', params);
});

router.post('/node/tabla', (req, res) => {
    const params = {
        numero: req.body.numero
    };
    res.render('node/tabla', params);
});

router.get('/node/cotizacion', (req, res) => {
    res.render('node/cotizacion');
});

router.post('/node/cotizacion', (req, res) => {
    const { precio, porcentajeInicial, plazo } = req.body;

    const pagoInicial = (parseFloat(porcentajeInicial) / 100) * parseFloat(precio);
    const totalFinanciar = parseFloat(precio) - pagoInicial;
    const pagoMensual = totalFinanciar / parseInt(plazo);
    res.render('node/cotizacion', { 
        porcentajeInicial,
        precio,
        plazo,
        pagoInicial,
        totalFinanciar,
        pagoMensual
    });
});



let rows;
router.get('/node/alumnos',async(req,res)=>{
rows = await alumnosDb.mostrarTodos();

res.render('node/alumnos',{reg:rows});

})
let params;
router.post('/node/alumnos', async(req,res)=>{
try {

params ={
matricula:req.body.matricula,
nombre:req.body.nombre,
domicilio:req.body.domicilio,
sexo : req.body.sexo,
especialidad:req.body.especialidad
}
const registros = await alumnosDb.insertar(params);
console.log("-------------- registros " + registros);

} catch(error){
console.error(error)
res.status(400).send("sucedio un error: " + error);
}
rows = await alumnosDb.mostrarTodos();
res.render('node/alumnos',{reg:rows});
});

async function prueba(){
     try{
        const res = await conexion.execute("select * from alumnos");
        console.log("El resultado es ",res);
     }catch(error){
        console.log("Surigio un error " ,error);
     }
     finally{

     }
}




router.post("/node/buscar",async(req,res)=>{
    matricula = req.body.matricula;

    row = await alumnosDb.consultarMatricula(matricula);
    res.render("node/alumnos",{alu:nrow});
})
router.get('/preexamen/recibo', (req, res) => {
    // Parámetros
    const params = {
        numeroRecibo: req.query.numeroRecibo,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        tipoServicio: req.query.tipoServicio,
        kw: req.query.kw,
    }
    res.render('preexamen/recibo', params);
});

router.post('/preexamen/recibo', (req, res) => {
    // Parámetros
    const params = {
        numeroRecibo: req.body.numeroRecibo,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        tipoServicio: req.body.tipoServicio,
        kw: req.body.kw,
    }
    res.render('preexamen/recibo', params);
});

export default (router);